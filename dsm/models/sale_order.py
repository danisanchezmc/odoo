# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import fields, models


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    fecha_fin = fields.Date(string='Fecha Fin')
    encargado = fields.Many2one(comodel_name="hr.employee", string="Encargado de proyecto")
    state = fields.Selection([
        ('draft', 'Quotation'),
        ('sent', 'Quotation Sent'),
        ('accepted', "Presupuesto Aceptado"),
        ('sale', 'Sales Order'),
        ('done', 'Locked'),
        ('cancel', 'Cancelled'),
        ], string='Status', readonly=True, copy=False, index=True, track_visibility='onchange', track_sequence=3, default='draft')

    def create_project(self):
        # cambiamos el estado del pedido a Aceptado
        self.write({'state': 'accepted', })

        # obtenemos las etapas de las tareas por defecto
        tareas = self.env['project.task.type'].search([
            ("name","in",["Para ejecutar",
                          "En proceso",
                          "Hecho",
                          "Cancelado",
                          ])
        ])

        # creamos el proyecto
        proyecto = self.env['project.project'].create({
            'name': "Proyecto " + self.name,
            'user_id': self.encargado.user_id.id,
            'partner_id': self.partner_id.id,
            'sale_order': self.id,
        })
        # 'type_ids': tareas.ids,
        tareas.update({
            'project_ids': [4, proyecto.id],
        })

        # creamos las tareas
        for line in self.order_line:
            if line.product_id.product_tmpl_id.crear_tarea:
                self.env['project.task'].create({
                    'project_id': proyecto.id,
                    'name': line.name,
                    'date_deadline': self.fecha_fin,
                    'sale_line_id': line.id,
                    'planned_hours': line.product_uom_qty,
                    'stage_id': tareas[0].id,
                })
