# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import fields, models, _


class ProjectProject(models.Model):
    _inherit = 'project.project'

    sale_order = fields.Many2one(comodel_name="sale.order", string='Pedido de venta', readonly="1")

    def end_project(self):

        # cambiamos el estado del pedido a Pedido de Venta
        self.sale_order.write({'state': 'sale', })

        # mostramos el pedido
        return {
            'name': _('Pedido de Venta'),
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'sale.order',
            'res_id': self.sale_order.id,
            'type': 'ir.actions.act_window',
        }