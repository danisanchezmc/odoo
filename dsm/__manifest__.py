# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'DSM',
    'version': '1.1',
    'category': 'Human Resources',
    'sequence': 75,
    'summary': 'Proyecto DSM',
    'description': "",
    'images': [
        'images/hr_department.jpeg',
        'images/hr_employee.jpeg',
        'images/hr_job_position.jpeg',
        'static/src/img/default_image.png',
    ],
    'depends': [
        'hr_timesheet',
        "sale",
    ],
    'data': [
        'views/sale_order_view.xml',
        'views/product_template_view.xml',
        'views/product_product_view.xml',
        'views/project_view.xml',
    ],
    'demo': [
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
    'qweb': [],
}
